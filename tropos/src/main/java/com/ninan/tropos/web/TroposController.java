package com.ninan.tropos.web;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class TroposController{

    @GetMapping("/hello")
    public String helloTropos(){
        log.info("calling hello world success");
        return "Hello Tropos";
    }

}