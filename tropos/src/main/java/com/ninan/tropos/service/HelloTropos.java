package com.ninan.tropos.service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HelloTropos{
    protected String run(){
        log.info("Hello Tropos Discovery Service");
        return "Hello";
    }
}